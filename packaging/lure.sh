name=scan-git
_name=scan
desc="Command line scanner of QR, barcodes or text in screen (OCR)"
version=0.1.0.r4.bfd46cd
release=1
license=('AGPL-3.0-or-later')
homepage='https://codeberg.org/tplasdio/scan'
maintainer="tplasdio <tplasdio cat codeberg dog com>"
deps=(
 imagemagick
 #graphicsmagick
 xdotool
 zbar
 tesseract
 slop
)
deps_debian=(
 imagemagick
 tesseract-ocr
 zbar-tools
 xdotool
 slop
)
deps_fedora=(
 ImageMagick
 tesseract
 zbar
 xdotool
 slop
)
deps_alpine=(
 imagemagick
 xdotool
 zbar
 #tesseract  # not in official repos?
 slop
)
build_deps=(
 make
)
architectures=('all')
sources=("git+$homepage")
checksums=('SKIP')

version() {
	cd "$srcdir/$_name"
	git-version
}

package() {
	cd "$srcdir/$_name"

	make install DESTDIR="$pkgdir" PREFIX=/usr PRG="$_pkgname" NOFAKEROOT=1
}
