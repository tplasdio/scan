<p align="center">
  <h1 align="center">Scan</h1>
  <p align="center">
    Scan QR, barcodes or text (OCR) in your screen
  </p>
</p>

## 📰 Description

Scan either QR, barcodes or text characters in your screen,
optionally selecting a specific region or window, copying
to clipboard and showing a notification.

Tested on X11 Linux.

## 🚀 Installation

First install [`getoptions`](https://github.com/ko1nksm/getoptions)

<details>
<summary>📦 From source</summary>

- *Dependencies*:
  - `sh`
  - ImageMagick
  - `zbarimg`
  - `tesseract`
  - `xdotool`
  - `slop`
  - [`getoptions`](https://github.com/ko1nksm/getoptions)

- *Build Dependencies*:
  - `gettext`, optional for translations

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/scan.git
cd scan
./make.sh install  # or
# make install
```

Run the `make.sh` as root for system-installation.

For uninstallation, run either `./make.sh uninstall` or `make uninstall`.

</details>

<details>
<summary>Arch Linux or other pacman distros</summary>

```sh
curl -O https://codeberg.org/tplasdio/scan/raw/branch/main/packaging/PKGBUILD-git
makepkg -sip PKGBUILD-git
```

For uninstallation, run `sudo pacman -Rsn scan-git`.

</details>

<details>
<summary>Debian, Ubuntu or other .deb distros</summary>

Install [lure](https://gitea.elara.ws/Elara6331/lure/releases/latest)

```sh
curl -O https://codeberg.org/tplasdio/scan/raw/branch/main/packaging/lure.sh
LURE_PKG_FORMAT=deb lure build
apt-get -f install
```

</details>

</details>

<details>
<summary>Fedora, OpenSUSE or other .rpm distros</summary>

Install [lure](https://gitea.elara.ws/Elara6331/lure/releases/latest)

```sh
curl -O https://codeberg.org/tplasdio/scan/raw/branch/main/packaging/lure.sh
LURE_PKG_FORMAT=rpm lure build
rpm -i scan*.rpm
```

</details>

<details>
<summary>Alpine Linux</summary>

Install [lure](https://gitea.elara.ws/Elara6331/lure/releases/latest)
and [tesseract](https://github.com/tesseract-ocr/tesseract)

```sh
curl -O https://codeberg.org/tplasdio/scan/raw/branch/main/packaging/lure.sh
LURE_DISTRO=alpine LURE_PKG_FORMAT=apk lure build
apk add --allow-untrusted scan*.apk
```

</details>

## ⭐ Usage
```sh
Usage:
  scan [-O] [-s] [-w] [-c [-n]] -- [OTHER_ARGS]

Options:
  -s, --select                Select screen region to scan
  -w, --window                Select window to scan
  -O, --ocr                   Scan text characters
  -c, --copy                  Copy to clipboard
  -n, --notify                Show a notification
  -V, --version               Show version
  -h, --help                  Show this help
```

## 📟 Examples

```sh
scan               # Scan QR/barcodes from entire screen
scan -O            # Scan text (OCR) from entire screen
scan -s            # Selecting screen region to scan from
scan -w            # Selecting window to scan from
scan -sw           # Selecting region or window to scan from
scan -swc          # Copying scanning to clipboard
scan -swcn         # Copying scanning to clipboard and showing notification
scan -- --raw      # Passing arguments to zbarimg
scan -O -- -l spa  # Passing arguments to tesseract
```

## 👀 See also

- [zbarimg(1)](https://manpages.debian.org/unstable/zbar-tools/zbarimg.1.en.html)
- [tesseract(1)](https://manpages.debian.org/unstable/tesseract-ocr/tesseract.1.en.html)
