all:

default: install

install:
	DESTDIR=$(DESTDIR) PRG=$(PRG) PREFIX=$(PREFIX) NOFAKEROOT=$(NOFAKEROOT) \
		./make.sh install

uninstall:
	DESTDIR=$(DESTDIR) PRG=$(PRG) PREFIX=$(PREFIX) NOFAKEROOT=$(NOFAKEROOT) \
		./make.sh uninstall

.PHONY: all default install uninstall
