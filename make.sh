#!/bin/sh

setup()
{
	: \
	"${DESTDIR:="/"}" \
	"${PRG:=scan}" \
	"${XDG_PREFIX:="${HOME}/.local"}"

	PRG_PATH=scan \
	PRG_NAME="${PRG_PATH##*/}" \
	LOCALE_PATH=locale

	[ "$(id -u)" -eq 0 ] \
		&& isroot=: \
		|| [ "$NOFAKEROOT" ] \
		&& isroot=: \
		|| isroot=false \
	# NOFAKEROOT option for forcing root installation, for certain
	# build systems with no fakeroot

	prefix="${PREFIX:=/usr/local}"
	$isroot || prefix="${XDG_PREFIX}"

	bindir="$prefix/bin" \
	localedir="$prefix/share/locale"

	hash gettext >/dev/null 2>&1 \
		&& hasgettext=: \
		|| hasgettext=false
}

install_translations()
{
	dirname_def='dirname(){ eval "$1"='\''"${'\''"$1"'\''%/*}"'\'' ;}'
	basename_def='basename(){ eval "$1"='\''"${'\''"$1"'\''##*/}"'\'' ;}'

	# foreach .po file
	find -name '*.po' -exec sh -s {} + <<-EOF
	set -x
	$dirname_def
	$basename_def

	for po_file do
		# get locale dir
		locale="\${po_file}"
		dirname locale; dirname locale; basename locale

		mkdir -p -- "${DESTDIR}/${localedir}/\${locale}/LC_MESSAGES/"
		msgfmt -o "${DESTDIR}/${localedir}/\${locale}/LC_MESSAGES/${PRG}.mo" "\${po_file}"
	done
	EOF
}

install()
{
	mkdir -p -- "${DESTDIR}/${bindir}" "${DESTDIR}/${localedir}"
	chmod 755 -- "${PRG_PATH}"
	cp -- "${PRG_PATH}" "${DESTDIR}/${bindir}/${PRG}"
	$hasgettext \
		&& install_translations \
		|| >&2 echo "$0: gettext not found, not installing translations"
}

uninstall()
{
	rm -f -- "${DESTDIR}/${bindir}/${PRG}"

	# foreach locale (e.g. es, de, fr)
	find "${LOCALE_PATH}" -maxdepth 1 \
		! -path "${LOCALE_PATH}" ! -path "${LOCALE_PATH}/${PRG_PATH}.pot" \
		-exec sh -s {} + \
	<<-EOF
	set -x
	for locale do
		locale="\${locale##*/}"
		rm -f -- "${DESTDIR}/${localedir}/\${locale}/LC_MESSAGES/${PRG}.mo"
	done
	EOF

}

main()
{
	set -x
	setup || return $?
	target="${1-install}"

	case "$target" in
	(i|install)   install;;
	(u|uninstall) uninstall;;
	(*)
		printf -- 'Unknown target given: "%s"\n' "$1"
		return 22
	esac
}

main "$@" || exit $?
